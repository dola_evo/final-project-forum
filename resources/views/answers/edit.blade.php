@extends('layouts/master')
@section('title')
    {{-- isi dengan judul content --}}
    <h1>Edit Answer</h1>
@endsection
@section('content')
    {{-- isi dengan isi content --}}
    <form action="/answer/{{ $answer->id }}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Judul Pertanyaan</label>
          <input type="text" class="form-control" disabled value="{{ $answer->question->judul }}">          
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Jawaban</label>
          <textarea class="form-control" name="isi" rows="3" >{{ $answer->isi }}</textarea>
        </div>
        <input type="hidden" name='users_id' value="{{ Auth::id() }}">
        <input type="hidden" name='questions_id' value="1">   
        @error('isi')
            <p class="text-danger">{{ $message }}</p>
        @enderror      
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    
@endsection