@extends('layouts/master')
@section('breadcumb')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0">Daftar Kategori</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Kategori</a></li>
            <li class="breadcrumb-item active">Daftar</li>
        </ol>
    </div>
</div>
@endsection
@section('title')
   Daftar Kategori
@endsection
@section('content')
<a href="/category/create" class="btn btn-success mb-3"><i class="fa fa-plus"></i> Tambah</a>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($category as $key=>$item)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$item->name}}</td>
                        <td>
                            <form action="/category/{{$item->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/category/{{$item->id}}/edit" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                <button type="submit" class="btn btn-danger mx-1"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@endpush
@push('scripts')       
    <script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush  