@extends('layouts/master')  
@section('breadcumb')
<div class="row mb-2">
    <div class="col-sm-6">
       
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Questions</a></li>
            <li class="breadcrumb-item active">Pages</li>
        </ol>
    </div>
</div>
@endsection
@section('title')
    <h4>Halaman Pertanyaan</h4>
@endsection

@section('content')
    <a href="/questions/create" class="btn btn-primary btn-sm my-3">Tambah Pertanyaan</a>
<div class="row">
@forelse ($questions as $key => $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('image/'.$item->gambar)}}" alt="">
            <div class="card-body">
              <h5>{{$item->judul}}</h5>
              <p class="card-text">{{Str::limit($item->isi, 20)}}</p>          
              <form action="/questions/{{$item->id}}" method="POST">
                <a href="/questions/{{$item->id}}" class="btn btn-info btn sm ml-2">Detail</a>
                @if ($item->users_id == $user->id)
                <a href=" questions/{{$item->id}}/edit" class="btn btn-warning btn sm ml-2">Edit</a>
                @endif
                @csrf
                @method('DELETE')
                @if ($item->users_id == $user->id)
                <input type="submit" class="btn btn-danger btn sm ml-2" value="Delete">
                @endif
                
            </form>
            </div>
          </div>
    </div>
@empty 
    <h6>Tidak ada pertanyaan</h6>
@endforelse
</div>
@endsection
@section('script')
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            Swal.fire({
                title: 'Success!',
                text: 'Berhasil Mengakses Halaman Pertanyaan',
                icon: 'success',
                confirmButtonText: 'oke'
            })
        });
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection