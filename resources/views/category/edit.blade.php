@extends('layouts/master')
@section('breadcumb')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0">Edit Kategori</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Kategori</a></li>
            <li class="breadcrumb-item active">Edit</li>
        </ol>
    </div>
</div>
@endsection
@section('title')
    Edit Kategori
@endsection
@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">
                <div class="card-title">Edit Kategori</div>
            </div>
            <form action="/category/{{$category->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Nama Kategori</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$category->name}}">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection