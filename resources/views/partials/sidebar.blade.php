<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <h1 class="d-flex justify-content-center text-light mt-3">FORUM</h1>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img
              src="{{ asset('template/avatar/male.png') }}"
              class="img-circle elevation-2"
              alt="User Image"
            />
          </div>
          <div class="info">
            @guest <a href="/login" class="d-block"> Anda Belum Login </a> @else <a href="/profile" class="d-block"> {{ Auth::user()->name }} </a> @endguest
          </div>
        </div>

      <!-- SidebarSearch Form -->
        <div class="form-inline">
          <div class="input-group" data-widget="sidebar-search">
            <input
              class="form-control form-control-sidebar"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <div class="input-group-append">
              <button class="btn btn-sidebar">
                <i class="fas fa-search fa-fw"></i>
              </button>
            </div>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-3">
          <ul
            class="nav nav-pills nav-sidebar flex-column"
            data-widget="treeview"
            role="menu"
            data-accordion="false"
          >
            <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
            <li class="nav-item">
              <a href="/" class="nav-link active">
                <i class="nav-icon fas fa-tachometer-alt"></i>                  
                <p>
                  Dashboard                    
                </p>
              </a>
            </li>
            <li class="nav-item menu-open">
              <a href="#" class="nav-link ">
                <i class="nav-icon fas fa-th"></i>
                <p>
                  Pages
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                @auth
                <li class="nav-item">
                  <a href="{{('category')}}" class="nav-link ">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Categories</p>
                  </a>
                </li>    
                @endauth
                <li class="nav-item">
                  <a href="{{('questions')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Questions</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{('answer')}}" class="nav-link ">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Answer</p>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>