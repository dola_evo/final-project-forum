@extends('layouts/master')
@section('title')
    {{-- isi dengan judul content --}}
    <h1>Index Answer</h1>
@endsection
@section('content')
    {{-- isi dengan isi content --}}
    <a href="/answer/create" class="btn btn-primary">Tambah</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">jawaban</th>
            <th scope="col">Action</th>
            
          </tr>
        </thead>
        <tbody>
         
          @forelse ($answer as $item)
          <tr>
            <th scope="row">{{ $loop->index+1 }}</th>
            <td>{{ $item->isi }}</td>
            <td> <form action="/answer/{{ $item->id }}" method="POST">
              @csrf
              @method('DELETE')
              <a href="/answer/{{ $item->id }}/edit" class="btn btn-warning">Edit</a>
              <button type="submit" class="btn btn-danger">Delete</button>
          </form>          
          </td>
          
          </tr>
          @empty
              tidak ada data
          @endforelse
          
          
        </tbody>
      </table>
@endsection