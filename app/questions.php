<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questions extends Model
{
    //
    protected $table = 'questions';
    protected $fillable = ['judul','isi','gambar','users_id','categories_id'];

            public function Category()
        {
            return $this->belongsTo('App\Category', 'categories_id');
        }

        public function User()
        {
            return $this->belongsTo('App\User', 'users_id');
        }
}
