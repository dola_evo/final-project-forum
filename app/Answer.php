<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['isi','users_id','questions_id'];
    protected $table = 'answers';
    public function user(){
        return $this->belongsTo('App\User','users_id');
    }
    public function question(){
        return $this->belongsTo('App\questions','questions_id');
    }
}
 