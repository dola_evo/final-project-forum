<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Category;
use App\questions;
use Illuminate\Support\Facades\Auth;
use File;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User();
        $questions = DB::table('questions')->get();
        return view('questions.index', compact('questions','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::User();
        $kategori= Category::all();
        return view('questions.create', compact('kategori', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'categories_id' => 'required',
            'gambar' => 'required',
        ]);
  
        $NamaGambar = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('image'), $NamaGambar);

        $questions = new questions;
 
        $questions->judul = $request->judul;
        $questions->isi = $request->isi;
        // $questions->users_id = $request->users_id ;
        $questions->users_id = request()->user()->id;
        $questions->categories_id = $request->categories_id;
        $questions->gambar = $NamaGambar;
        
        $questions->save();
        return redirect('/questions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $questions = questions::find($id);
        return view('questions.detail', compact('questions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::User();
        $kategori = Category::all();
        $questions = questions::find($id);
        return view('questions.update', compact('questions','kategori','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'categories_id' => 'required',
            'gambar' => '',
        ]);
        
        $questions = questions::find($id);
        if ($request->has('gambar')) {

            $path ="image/";
            File::delete($path . $questions->gambar);

            $NamaGambar = time().'.'.$request->gambar->extension();  
   
            $request->gambar->move(public_path('image'), $NamaGambar);

            $questions->gambar =$NamaGambar;
            $questions->save();
        }

        $questions->judul = $request->judul;
        $questions->isi = $request->isi;
        $questions->users_id = request()->user()->id;
        $questions->categories_id = $request->categories_id;
        
        $questions->save();
        return redirect('/questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questions = questions::find($id);
        $path ="image/";
        File::delete($path . $questions->gambar);
        $questions->delete();
        return redirect('/questions');
    }
}
