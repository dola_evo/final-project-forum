<?php
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// ===========  route untuk table question ===============
Route::resource('questions','QuestionsController');








// ===========  route untuk table answers ===============
Route::resource('answer', 'AnswerController');








// ===========  route untuk table profiles dan categories ===============
Route::group(['middleware' => ['auth']], function(){
Route::resource('category','CategoryController');
});

Route::resource('profile','ProfileController')->only([
    'index','update'
]);










