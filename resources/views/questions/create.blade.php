@extends('layouts/master')
@section('breadcumb')
<div class="row mb-2">
    <div class="col-sm-6">
       
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Pertanyaan</a></li>
            <li class="breadcrumb-item active">Tambah</li>
        </ol>
    </div>
</div>
@endsection
@section('title')
    Menambah Pertanyaan
@endsection
@section('content')
<form action="/questions" method="POST" enctype="multipart/form-data">
  
    @csrf
    <label> User</label>
      <input type="text" name="users_id" value="{{$user->name}}" disabled class="form-control">
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" value="{{old('judul')}}" class="form-control">
    </div>
        @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Pertanyaan</label>
      <textarea name="isi" cols="30" rows="10"value="{{old('judul')}}" class="form-control"></textarea>
    </div>
    @error('isi')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
        <label>Kategori</label>
        <select name="categories_id" id="" class="form-control">
          <option value="">--Pilih salah satu kategori--</option>
          @forelse ($kategori as $item)
          <option value="{{$item->id}}">{{$item->name}}</option>
          @empty
          <option value="">Tidak ada pertanyaan</option>
          @endforelse
        </select>
    </div>
      @error('categories_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
        <label>Gambar</label>
        <input type="file" name="gambar" class="form-control" id="">
    </div>
      @error('gambar')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection