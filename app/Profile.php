<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $fillable = ['biodata','umur','alamat','users_id'];
    
    public function user(){
        return $this->belongsTo('App\User', 'users_id');
    }
}
