@extends('layouts/master')
@section('title')
    {{-- isi dengan judul content --}}
    <h1>Home</h1>
@endsection
@section('content')
    {{-- isi dengan isi content --}}
    <form action="/answer" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Judul Pertanyaan</label>
            <input type="text" class="form-control"  value="{{ $answer->question->judul }}" disabled>            
          </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Jawaban</label>
          <textarea class="form-control" name="isi" rows="3" disabled >{{ $answer->isi }}</textarea>
        </div>
        <input type="hidden" name='users_id' value="{{ Auth::id() }}">
        <input type="hidden" name='questions_id' value="1">   
        @error('isi')
            <p class="text-danger">{{ $message }}</p>
        @enderror      
        <div class="card-footer">
            <form action="/answer/{{ $answer->id }}" method="POST">
                @csrf
                @method('DELETE')
                <a href="/answer/{{ $answer->id }}/edit" class="btn btn-warning">Edit</a>
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>          
        </div>
      </form>
@endsection