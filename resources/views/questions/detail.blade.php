@extends('layouts/master')  
@section('breadcumb')
<div class="row mb-2">
    <div class="col-sm-6">
       
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Questions</a></li>
            <li class="breadcrumb-item active">Pages</li>
        </ol>
    </div>
</div>
@endsection
@section('title')
    <h4>Halaman Detail Pertanyaan</h4>
@endsection

@section('content')
    
        <div class="card" >
            <img src="{{asset('image/'. $questions->gambar)}}" class="card-img-top" style="width: 100vh; height:400px;" alt="">
            <div class="card-body">
              <h5>{{$questions->judul}}</h5>
              <p class="card-text">{{$questions->isi, 20}}</p>
                <a href="/questions" class="btn btn-info btn sm ml-2">Kembali</a>
            </div>
        </div>


@endsection