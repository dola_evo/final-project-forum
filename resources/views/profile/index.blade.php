@extends('layouts/master')
@section('breadcumb')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0">Edit Data Profile</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Profile</a></li>
            <li class="breadcrumb-item active">Edit Data</li>
        </ol>
    </div>
</div>
@endsection
@section('title')
    Edit Data Profile
@endsection
@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <div class="card-title">Profile</div>
            </div>
            <form action="/profile/{{$profile->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" value="{{$profile->user->name}}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" value="{{$profile->user->email}}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="biodata">Biodata</label>
                        <textarea class="form-control" id="biodata" name="biodata">{{$profile->biodata}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="number" class="form-control" id="umur" name="umur" value="{{$profile->umur}}">
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea class="form-control" id="alamat" name="alamat">{{$profile->alamat}}</textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection