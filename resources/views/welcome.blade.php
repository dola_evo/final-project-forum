@extends('layouts/master')
@section('breadcumb')
{{-- Ganti sesuai nama content breadcumb --}}
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0">Home</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li></li>
        </ol>
    </div>
</div>
@endsection
@section('title')
    {{-- isi dengan judul content --}}
    <h1>Home</h1>
@endsection
@section('content')
    {{-- isi dengan isi content --}}
    <h3>Selamat Datang Di Forum kami</h3>
@endsection
