<?php

namespace App\Http\Controllers;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    public function index(){
        $id = Auth::id();

        $profile = Profile::where('users_id', $id)->first();

        return view('profile.index', compact('profile'));
    }

    public function update(Request $request, $id){

        $request->validate([
            'biodata' => 'required',
            'umur' => 'required',
            'alamat' => 'required'
        ]);

        $profile = Profile::find($id);

        $profile->biodata = $request->biodata;
        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->save();

        Alert::success('Berhasil', 'Berhasil Mengedit Profile');
        return redirect('profile');

    }
}
