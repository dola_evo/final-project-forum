<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;


class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $answer = Answer::all();        
        return view('answers/index',compact('answer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('answers/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'isi' => 'required',
        ],[
            'isi.required' => 'jawaban tidak boleh kosong',
        ]);

        $answer = new Answer;
        $answer->questions_id = $request->questions_id;
        $answer->users_id = $request->users_id;
        $answer->isi=$request->isi;
        $answer->save();

        return redirect('/answer');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $answer = Answer::find($id);
        return view('answers/show',compact('answer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $answer = Answer::find($id);

        return view('answers/edit',compact('answer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'isi' => 'required',
        ],[
            'isi.required' => 'jawaban tidak boleh kosong',
        ]);
        $answer = Answer::find($id);
        $answer->isi = $request->isi;
        $answer->save();
        // $url = 'answer/'.$answer->id;
        
        return redirect('/answer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Answer::destroy($id);
        return redirect('answer');
    }
}
