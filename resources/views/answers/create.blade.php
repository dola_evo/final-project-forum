@extends('layouts/master')
@section('title')
    {{-- isi dengan judul content --}}
    <h1>Home</h1>
@endsection
@section('content')    
        <form action="/answer" method="post">
          @csrf
          <div class="form-group">
            <label for="exampleFormControlTextarea1">Jawaban</label>
            <textarea class="form-control" name="isi" rows="3"></textarea>
          </div>
          <input type="hidden" name='users_id' value="{{ Auth::id() }}">
          <input type="hidden" name='questions_id' value="1">   
          @error('isi')
              <p class="text-danger">{{ $message }}</p>
          @enderror      
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Tambah</button>
          </div>
        </form>
      

</form>
@endsection